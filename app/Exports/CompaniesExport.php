<?php

namespace App\Exports;

use App\Models\Companies;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CompaniesExport implements FromCollection, WithCustomCsvSettings, WithHeadings
{
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }

    public function headings(): array
    {
        return ["Company Id", "Company Name", "Email", "Website", "Created At", "Updated At"];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Companies::all();
    }
}
