<?php

namespace App\Exports;

use App\Models\Employees;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EmployeesExport implements FromCollection, WithCustomCsvSettings, WithHeadings
{
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }

    public function headings(): array
    {
        return ["Employee Id", "First Name", "Last Name", "Company Id", "Email", "Phone", "Created At", "Updated At"];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Employees::all();
    }
}
