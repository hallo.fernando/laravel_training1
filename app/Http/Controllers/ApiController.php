<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Companies;
use App\Models\Employees;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

use Auth;

class ApiController extends Controller
{
    // protected $user;

    // public function __construct()
    // {
    //     $this->user = JWTAuth::parseToken()->authenticate();
    // }

    public function show_companies_employees_list(){
        $companies = Companies::select('*', 'employees.firstname', 'employees.lastname', 'employees.email', 'employees.phone')
        ->join('employees','employees.companyId', '=', 'companies.companyId')
        ->select('companies.companyId', 'companies.companyName', 'employees.firstname', 'employees.lastname', 'employees.email', 'employees.phone')
        ->get();

        return response()->json($companies, 200);
    }

    public function show($id)
    {
        $companies = Companies::select('*', 'employees.firstname', 'employees.lastname')
        ->join('employees','employees.companyId', '=', 'companies.companyId')
        ->select('companies.companyId', 'companies.companyName', 'employees.firstname', 'employees.lastname')
        ->find([$id]);

        return response()->json($companies, 200);
    }
}
