<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use Carbon\Carbon;

use Spatie\Image\Image;

use Illuminate\Support\Facades\Session;
use App\Http\Requests\CompaniesRequest;

use App\Models\Companies;
use App\Models\User;
use App\Imports\CompaniesImport;
use App\Exports\CompaniesExport;
use Maatwebsite\Excel\Facades\Excel;
use File;

class CompaniesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        if (request()->start_date || request()->end_date) {
            $start_date = Carbon::parse(request()->start_date)
            ->timezone(Session::get('time'))
            ->toDateTimeString();
            $end_date = Carbon::parse(request()->end_date)
            ->timezone(Session::get('time'))
            ->toDateTimeString();

            $Optcompanies = Companies::all();

            $Semail = (request('Semail'));
            $Scompanies = (request('companies'));
            $Swebsite = (request('Swebsite'));
                
            $companies = Companies::whereBetween('created_at',[$start_date,$end_date])
            ->where('email', 'LIKE', "%{$Semail}%")
            ->where('companyName', 'LIKE', "%{$Scompanies}%")
            ->where('website', 'LIKE', "%{$Swebsite}%")
            ->get();

            return view('companies/index', ['companies' => $companies, 'Optcompanies' => $Optcompanies]);
        }

        $Optcompanies = Companies::all();

        $Semail = (request('Semail'));
        $Scompanies = (request('companies'));
        $Swebsite = (request('Swebsite'));
        $companies = Companies::select('*')
        ->where('email', 'LIKE', "%{$Semail}%")
        ->where('companyName', 'LIKE', "%{$Scompanies}%")
        ->where('website', 'LIKE', "%{$Swebsite}%")
        ->get();

        return view('companies/index', ['companies' => $companies, 'Optcompanies' => $Optcompanies]);
    }

    public function create(){
        $companies = Companies::all();
        return view('companies/create', ['companies' => $companies]);
    }
    
    public function store(CompaniesRequest $request){
        if($request->logo){
            $file = $request->file('logo');
            // $nama_file = time()."_".$file->getClientOriginalName();
            // $tujuan_upload = 'storage/logo';
            // $file->storeAs('public/logo',$nama_file);
            // $file->move($tujuan_upload,$nama_file);

            // Logo Testing
            $file->store('logo','public');

            // Resize Logo
            Image::load(public_path('storage/logo/'). $file->hashName())
            ->width(100)
            ->height(100)
            ->save();

            $files = $file->hashName();

        }else{
            $files = NULL;
        }

        $companies = Companies::create([
            'companyName' => $request->companyName,
            'email' => $request->email,
            'website' => $request->website,
            'logo' => $files,
            'created_by_id' => $request->created_by_id,
            'updated_by_id' => $request->updated_by_id
        ]);
        
        return redirect('companies/');
    }

    public function edit($id){
        $companies = Companies::find($id);
        return view('companies/edit', ['companies' => $companies]);
    }

    public function update($id, CompaniesRequest $request){
        $companies = Companies::find($id);
        
        if($request->hasFile('logo')){
            $file_exist = storage_path('app/public/logo/') . $companies->logo;
            if(File::exists($file_exist)){
                File::delete($file_exist);
            }
            $file = $request->file('logo');
            // $nama_file = time()."_".$file->getClientOriginalName();
            // $tujuan_upload = 'storage/logo/';
            // $file->move($tujuan_upload,$nama_file);

            $file->store('logo','public');
            Image::load(public_path('storage/logo/'). $file->hashName())
            ->width(100)
            ->height(100)
            ->save();

            $files = $file->hashName();
        }
        else{
            $files = $companies->logo;
        }

        $companies->companyName = $request->companyName;
        $companies->email = $request->email;
        $companies->website = $request->website;
        $companies->logo = $files;
        $companies->updated_by_id = $request->updated_by_id;
        $companies->save();
        return redirect('companies/');
    }

    public function delete($id){
        $companies = Companies::find($id);
        $file_exist = storage_path('app/public/logo/') . $companies->logo;
        if(File::exists($file_exist)){
            File::delete($file_exist);
        }
        $companies->delete();
        return redirect('companies/');
    }

    public function export()
	{
		return Excel::download(new CompaniesExport, 'companies.xlsx');
	}
 
	public function import(Request $request) 
	{
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);

        $file = $request->file('file');

        // Excel Testing
        $file->store('companies_files','public');
 
		// $file = $request->file('file');
 
		// $nama_file = rand().$file->getClientOriginalName();
 
		// $file->move('companies_files',$nama_file);
 
		// Excel::import(new CompaniesImport, public_path('/companies_files/'.$nama_file));
		Excel::import(new CompaniesImport, public_path('storage/companies_files/'.$file->hashName()));
 
		// Session::flash('sukses','Data Companies Berhasil Diimport!');
 
		return redirect('/companies');
	}



	//backup search
	// public function index(Request $request){
    //     $companies = Companies::all();
	// 	$companies = Companies::paginate(5);

	// 	$search =  $request->input('q');
    //     if($search!=""){
    //         $companies = Companies::where(function ($query) use ($search){
    //             $query->where('companyName', 'like', '%'.$search.'%')
    //                 ->orWhere('email', 'like', '%'.$search.'%')
    //                 ->orWhere('website', 'like', '%'.$search.'%');
    //         })
    //         ->paginate(10);
    //         $companies->appends(['q' => $search]);
    //     }
    //     else{
    //         $companies = Companies::paginate(10);
    //     }
    //     return view('companies/index', [
	// 		'companies' => $companies
	// 	])->with('data',$companies);
        


    //     // return view('companies/index', ['companies' => $companies]);
    // }
}
