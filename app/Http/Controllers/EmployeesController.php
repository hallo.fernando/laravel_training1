<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EmployeesCreateRequest;
use App\Http\Requests\EmployeesUpdateRequest;

use Session;
use Carbon\Carbon;

use App\Models\Employees;
use App\Models\Companies;
use App\Imports\EmployeesImport;
use App\Exports\EmployeesExport;
use Maatwebsite\Excel\Facades\Excel;

class EmployeesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        if (request()->start_date || request()->end_date) {
            $start_date = Carbon::parse(request()->start_date)
            ->timezone(Session::get('time'))
            ->toDateTimeString();
            $end_date = Carbon::parse(request()->end_date)
            ->timezone(Session::get('time'))
            ->toDateTimeString();

            $Optcompanies = Companies::all();

            $Sfirstname = (request('Sfirstname'));
            $Slastname = (request('Slastname'));
            $Semail = (request('Semail'));
            $Sphone = (request('Sphone'));
                
            $employees = Employees::whereBetween('created_at',[$start_date,$end_date])
            ->where('firstname', 'LIKE', "%{$Sfirstname}%")
            ->where('lastname', 'LIKE', "%{$Slastname}%")
            ->where('email', 'LIKE', "%{$Semail}%")
            ->where('phone', 'LIKE', "%{$Sphone}%")
            ->whereHas('companies', function($query){
                $Scompanies = (request('companies'));
                $query->where('companyName', 'LIKE', "%{$Scompanies}%");
            })
            ->get();

            return view('employees/index', [
                'employees' => $employees,
                'companies' => Companies::all(),
                'Optcompanies' => $Optcompanies
            ]);
        }

        $Optcompanies = Companies::all();

        $Sfirstname = (request('Sfirstname'));
        $Slastname = (request('Slastname'));
        $Semail = (request('Semail'));
        $Sphone = (request('Sphone'));
        $employees = Employees::select('*')
        ->where('firstname', 'LIKE', "%{$Sfirstname}%")
        ->where('lastname', 'LIKE', "%{$Slastname}%")
        ->where('email', 'LIKE', "%{$Semail}%")
        ->where('phone', 'LIKE', "%{$Sphone}%")
        ->whereHas('companies', function($query){
            $Scompanies = (request('companies'));
            $query->where('companyName', 'LIKE', "%{$Scompanies}%");
        })
        ->get();
        
		return view('employees/index', [
			'employees' => $employees,
			'companies' => Companies::all(),
            'Optcompanies' => $Optcompanies
		]);
    }

	public function create(){
        $companies = Companies::all();
        return view('employees/create', ['companies' => $companies]);
    }
    
    public function store(EmployeesCreateRequest $request){
        Employees::create([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'companyId' => $request->companyId,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($request->password),
            'created_by_id' => $request->created_by_id,
            'updated_by_id' => $request->updated_by_id
        ]);

        return redirect('employees/');
    }

    public function edit($id){
        $employees = Employees::find($id);
        $companies = Companies::all();

        return view('employees/edit', ['employees' => $employees, 'companies' => $companies]);
    }

    public function update($id, EmployeesUpdateRequest $request){
        $employees = Employees::find($id);
        if($request->password){
            $employees->firstname = $request->firstname;
            $employees->lastname = $request->lastname;
            $employees->companyId = $request->companyId;
            $employees->email = $request->email;
            $employees->password = bcrypt($request->password);
            $employees->phone = $request->phone;
            $employees->updated_by_id = $request->updated_by_id;
            $employees->save();
            return redirect('employees/');
        }
            $employees->firstname = $request->firstname;
            $employees->lastname = $request->lastname;
            $employees->companyId = $request->companyId;
            $employees->email = $request->email;
            $employees->phone = $request->phone;
            $employees->updated_by_id = $request->updated_by_id;
            $employees->save();
            return redirect('employees/');
    }

    public function delete($id){
        $employees = Employees::find($id);
        $employees->delete();
        return redirect('employees/');
    }

    public function export()
	{
		return Excel::download(new EmployeesExport, 'employees.xlsx');
	}
 
	public function import(Request $request) 
	{
        $this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);

        $file = $request->file('file');

        // Excel Testing
        $file->store('employees_files','public');

		// $file = $request->file('file');
 
		// $nama_file = rand().$file->getClientOriginalName();
 
		// $file->move('employees_files',$nama_file);
 
		// Excel::import(new EmployeesImport, public_path('/employees_files/'.$nama_file));
		Excel::import(new EmployeesImport, public_path('storage/employees_files/'.$file->hashName()));

 
		// Session::flash('sukses','Data Employees Berhasil Diimport!');
 
		return redirect('/employees');
	}




	// Backup Index
	// public function index(Request $request){
    //     $employees = Employees::select('*', 'companies.companyName')
    //     ->join('companies','companies.companyId', '=', 'employees.companyId')
    //     ->select('employees.*','companies.companyName')
    //     ->get();

	// 	// $employees = Employees::select('*', 'companies.companyName')
    //     // ->join('companies','companies.companyId', '=', 'employees.companyId')
    //     // ->select('employees.*','companies.companyName')
    //     // ->paginate(5);

    //     return view('employees/index', ['employees' => $employees]);
    // }
}
