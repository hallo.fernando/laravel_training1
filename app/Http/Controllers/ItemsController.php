<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ItemsRequest;

use Session;

use App\Models\Items;
Use Carbon\Carbon;

class ItemsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        if (request()->start_date || request()->end_date) {
            $start_date = Carbon::parse(request()->start_date)
            ->timezone(Session::get('time'))
            ->toDateTimeString();
            $end_date = Carbon::parse(request()->end_date)
            ->timezone(Session::get('time'))
            ->toDateTimeString();

            $Sname = (request('Sname'));
            $Sprice = (request('Sprice'));

            $items = Items::whereBetween('created_at',[$start_date,$end_date])
            ->where('name', 'LIKE', "%{$Sname}%")
            ->where('price', 'LIKE', "%{$Sprice}%")
            ->get();

            return view('items/index', ['items' => $items]);
        }

        $Sname = (request('Sname'));
        $Sprice = (request('Sprice'));
        $items = Items::select('*')
        ->where('name', 'LIKE', "%{$Sname}%")
        ->where('price', 'LIKE', "%{$Sprice}%")
        ->get();

        return view('items/index', ['items' => $items]);
    }

    public function create(){
        return view('items/create');
    }
    
    public function store(ItemsRequest $request){
        Items::create([
            'name' => $request->name,
            'price' => $request->price,
        ]);

        return redirect('items/');
    }

    public function edit($id){
        $items = Items::find($id);

        return view('items/edit', ['items' => $items]);
    }

    public function update($id, ItemsRequest $request){
        $items = Items::find($id);
        $items->name = $request->name;
        $items->price = $request->price;
        $items->save();
        return redirect('items/');
    }

    public function delete($id){
        $items = Items::find($id);
        $items->delete();
        return redirect('items/');
    }
}
