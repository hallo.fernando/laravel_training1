<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jobs\SendEmailJob;

class JobController extends Controller
{
    public function enqueue(Request $request)
    {
        $details = ['email' => 'recipient@example.com'];
        SendEmailJob::dispatch($details);
        return "Success";
    }
}
