<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SalesRequest;

use Session;

Use Carbon\Carbon;

use App\Models\Sells;
use App\Models\Summaries;
use App\Models\Items;
use App\Models\Employees;

class SellsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        if (request()->start_date || request()->end_date) {
            $start_date = Carbon::parse(request()->start_date)
            ->timezone(Session::get('time'))
            ->toDateTimeString();
            $end_date = Carbon::parse(request()->end_date)
            ->timezone(Session::get('time'))
            ->toDateTimeString();

            $Sprice = (request('Sprice'));
            $Sdiscount = (request('Sdiscount'));
            
            $sells = Sells::whereBetween('date',[$start_date,$end_date])
            ->where('price', 'LIKE', "%{$Sprice}%")
            ->where('discount', 'LIKE', "%{$Sdiscount}%")
            ->whereHas('items', function($query){
                $Sname = (request('Sname'));
                $query->where('name', 'LIKE', "%{$Sname}%");
            })
            ->whereHas('employees', function ($query2){
                $Semployees = (request('Semployees'));
                $query2->where('firstname', 'LIKE', "%{$Semployees}%");
            })
            ->orderByDesc('date')
            ->get();
            return view('sales/index', ['sells' => $sells]);
        }

        $Sprice = (request('Sprice'));
        $Sdiscount = (request('Sdiscount'));
        $sells = Sells::select('*')
        ->where('price', 'LIKE', "%{$Sprice}%")
        ->where('discount', 'LIKE', "%{$Sdiscount}%")
        ->whereHas('items', function($query){
            $Sname = (request('Sname'));
            $query->where('name', 'LIKE', "%{$Sname}%");
        })
        ->whereHas('employees', function ($query2){
            $Semployees = (request('Semployees'));
            $query2->where('firstname', 'LIKE', "%{$Semployees}%");
        })
        ->orderByDesc('date')
        ->get();

        return view('sales/index', ['sells' => $sells]);
    }

    public function create(){
        $employees = Employees::all();
        $items = Items::all();

        return view('sales/create', ['employees' => $employees, 'items' => $items]);
    }
    
    public function store(SalesRequest $request){
        Sells::create([
            'date' => $request->date,
            'itemId' => $request->itemId,
            'price' => $request->price,
            'discount' => $request->discount,
            'employeeId' => $request->employeeId,
        ]);

        // $dates = Carbon::now();
        // $dates->toDateString();

        //search function 1 row

        $summaries = Summaries::where('date', '=', Carbon::parse($request->date)->format('Y,m,d'))->where('employeeId', '=', $request->employeeId)->first();
        $pricetotal = empty($summaries)? 0 : $summaries->price_total;
        $finalpricetotal = $pricetotal + $request->price;
        $discountTotal = empty($summaries)? 0 : $summaries->discount_total;
        $finaldiscountTotal = $discountTotal + ($request->price * $request->discount / 100);
        $total = $finalpricetotal - $finaldiscountTotal;

        Summaries::updateOrCreate(
            ['date' => Carbon::parse($request->date)->format('Y-m-d'), 'employeeId' => $request->employeeId],
            [
            'price_total' => $finalpricetotal,
            'discount_total' => $finaldiscountTotal,
            'total' => $total
        ]);

        return redirect('sales/');
    }

    public function edit($id){
        $sells = Sells::find($id);
        $employees = Employees::all();
        $items = Items::all();

        return view('sales/edit', ['sells' => $sells, 'employees' => $employees, 'items' => $items]);
    }

    public function update($id, SalesRequest $request){
        $sells = Sells::find($id);
        $sells->date = $request->date;
        $sells->itemId = $request->itemId;
        $sells->price = $request->price;
        $sells->discount = $request->discount;
        $sells->employeeId = $request->employeeId;
        $sells->save();
        return redirect('sales/');
    }

    public function delete($id){
        $sells = Sells::find($id);
        $sells->delete();
        return redirect('sales/');
    }
}
