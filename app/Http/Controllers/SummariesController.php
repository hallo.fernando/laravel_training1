<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;

use Session;
use Carbon\Carbon;

use App\Models\Summaries;
use App\Models\Sells;
use App\Models\Items;
use App\Models\Employees;
use App\Models\Companies;

class SummariesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        if (request()->start_date || request()->end_date) {
            $start_date = Carbon::parse(request()->start_date)
            ->timezone(Session::get('time'))
            ->toDateTimeString();
            $end_date = Carbon::parse(request()->end_date)
            ->timezone(Session::get('time'))
            ->toDateTimeString();

            $Optcompanies = Companies::all();
            
            $summaries = Summaries::whereBetween('date',[$start_date,$end_date])
            ->whereHas('employees', function($query){
                $query->whereHas('companies', function ($query2){
                    $Scompanies = (request('Scompanies'));
                    $query2->where('companyName', 'LIKE', "%{$Scompanies}%");
                });
            })
            ->whereHas('employees', function($query3){
                $Sfirstname = (request('Sfirstname'));
                $query3->where('firstname', 'LIKE', "%{$Sfirstname}%");
            })
            ->whereHas('employees', function($query4){
                $Slastname = (request('Slastname'));
                $query4->where('lastname', 'LIKE', "%{$Slastname}%");
            })
            ->orderByDesc('date')
            ->get();

            return view('summaries/index', [
                'summaries' => $summaries,
                'employees' => Employees::all(),
                'companies' => Companies::all(),
                'Optcompanies' => $Optcompanies
            ]);
        }

        $Optcompanies = Companies::all();

        $summaries = Summaries::select('*')
        ->whereHas('employees', function($query){
            $query->whereHas('companies', function ($query2){
                $Scompanies = (request('Scompanies'));
                $query2->where('companyName', 'LIKE', "%{$Scompanies}%");
            });
        })
        ->whereHas('employees', function($query3){
            $Sfirstname = (request('Sfirstname'));
            $query3->where('firstname', 'LIKE', "%{$Sfirstname}%");
        })
        ->whereHas('employees', function($query4){
            $Slastname = (request('Slastname'));
            $query4->where('lastname', 'LIKE', "%{$Slastname}%");
        })
        ->orderByDesc('date')
        ->get();

        return view('summaries/index', [
            'summaries' => $summaries,
			'employees' => Employees::all(),
			'companies' => Companies::all(),
            'Optcompanies' => $Optcompanies
		]);

    }

    public function view($id){
        $summaries = Summaries::find($id);
        $sells = Sells::with(['items'])->whereDate('date', '=', Carbon::parse($summaries->date)->format('Y-m-d'))->where('employeeId', '=', $summaries->employeeId)->get();
        return view('summaries/view', ['summaries' => $summaries, 'sells' => $sells]);
    }
}
