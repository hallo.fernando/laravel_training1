<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class TimezoneController extends Controller
{
    public function changetime($time)
    {
        $value = Str_replace(' ', '/', $time);
        Session::put('time', $value);
        return back();
    }
}
