<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompaniesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'companyName' => 'required',
            'email' => 'required',
            // 'email' => 'required|unique:companies'.$this->companies->companyId,
            'website' => 'required',
            'logo' => 'mimes:jpg,png'
        ];
    }
}
