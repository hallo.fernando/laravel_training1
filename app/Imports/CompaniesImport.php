<?php

namespace App\Imports;

use App\Models\Companies;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CompaniesImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Companies([
            'companyName' => $row['companyname'],
            'email' => $row['email'],
            'website' => $row['website'],
            'created_by_id' => $row['created_by_id'],
            'updated_by_id' => $row['updated_by_id']
        ]);
    }
}
