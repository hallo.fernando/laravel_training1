<?php

namespace App\Imports;

use App\Models\Employees;
use App\Models\Companies;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EmployeesImport implements ToModel, WithHeadingRow
{
    protected $company;

    public function __construct(){
        $this->company = Companies::select('companyId', 'companyName')->get();
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $companies = $this->company->where('companyName', $row['companyname'])->first();
        return new Employees([
            'firstname' => $row['firstname'],
            'lastname' => $row['lastname'],
            'companyId' => $companies->companyId,
            'email' => $row['email'],
            'phone' => $row['phone'],
            'created_by_id' => $row['created_by_id'],
            'updated_by_id' => $row['updated_by_id']
        ]);
    }
}
