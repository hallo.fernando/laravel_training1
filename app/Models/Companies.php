<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Illuminate\Support\Facades\Session;

class Companies extends Model
{
    use HasFactory;
    protected $table = 'companies';
    protected $primaryKey = 'companyId';

    // protected $fillable = ['companyName','email','website'];
    protected $fillable = ['companyName','email','website','logo','created_by_id','updated_by_id'];

    public function employee(){
        return $this->hasOne('App\Models\Employees', 'employeeId');
    }

    public function users(){
        return $this->belongsTo('App\Models\User', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone(Session::get('time'))
            ->toDateTimeString();
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone(Session::get('time'))
            ->toDateTimeString();
    }
}
