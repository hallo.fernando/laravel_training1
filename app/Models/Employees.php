<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Illuminate\Support\Facades\Session;

class Employees extends Model
{
    use HasFactory;
    protected $table = 'employees';
    protected $primaryKey = 'employeeId';

    // protected $fillable = ['firstname','lastname','companyId','email','phone'];
    protected $fillable = ['firstname','lastname','companyId','email', 'password','phone', 'created_by_id', 'updated_by_id'];

    public function companies(){
        return $this->belongsTo('App\Models\Companies','companyId');
    }

    public function users(){
        return $this->belongsTo('App\Models\User','id');
    }

    public function sells(){
        return $this->hasOne('App\Models\Sells', 'sellId');
    }

    public function summary(){
        return $this->hasOne('App\Models\Summaries', 'summariesId');
    }

    public function scopeSearch($query, $search)
    {
        return $query->where('companyName', 'LIKE', "%{$search}%");
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone(Session::get('time'))
            ->toDateTimeString();
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone(Session::get('time'))
            ->toDateTimeString();
    }
}
