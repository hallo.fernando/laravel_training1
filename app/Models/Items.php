<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Illuminate\Support\Facades\Session;

class Items extends Model
{
    use HasFactory;
    protected $table = 'items';
    protected $primaryKey = 'itemId';

    protected $fillable = ['name','price'];

    public function sells(){
        return $this->hasMany('App\Models\Sells', 'sellId');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone(Session::get('time'))
            ->toDateTimeString();
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone(Session::get('time'))
            ->toDateTimeString();
    }
}
