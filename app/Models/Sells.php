<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Illuminate\Support\Facades\Session;

class Sells extends Model
{
    use HasFactory;
    protected $table = 'sells';
    protected $primaryKey = 'sellId';

    protected $fillable = ['date','itemId','price','discount','employeeId'];

    public function items(){
        return $this->belongsTo('App\Models\Items','itemId');
    }

    public function employees(){
        return $this->belongsTo('App\Models\Employees', 'employeeId');
    }

    // public function getDateAttribute($value)
    // {
    //     return Carbon::createFromTimestamp(strtotime($value))
    //         ->timezone(Session::get('time'))
    //         ->toDateTimeString();
    // }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone(Session::get('time'))
            ->toDateTimeString();
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone(Session::get('time'))
            ->toDateTimeString();
    }
}
