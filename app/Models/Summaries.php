<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Illuminate\Support\Facades\Session;

class Summaries extends Model
{
    use HasFactory;
    protected $table = 'summaries';
    protected $primaryKey = 'summariesId';

    protected $fillable = ['date','employeeId','created_date','last_update','price_total', 'discount_total', 'total'];
    
    public function employees(){
        return $this->belongsTo('App\Models\Employees', 'employeeId');
    }

    public function sells(){
        return $this->belongsTo('App\Models\Sells', 'sellId');
    }

    public function items(){
        return $this->belongsTo('App\Models\Items', 'itemId');
    }

    public function companies(){
        return $this->belongsTo('App\Models\Companies', 'companyId');
    }

    // public function getDateAttribute($value)
    // {
    //     return Carbon::createFromTimestamp(strtotime($value))
    //         ->timezone(Session::get('time'))
    //         ->toDateTimeString();
    // }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone(Session::get('time'))
            ->toDateTimeString();
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone(Session::get('time'))
            ->toDateTimeString();
    }
}
