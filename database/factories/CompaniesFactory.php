<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Companies;
use App\Models\User;

class CompaniesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = Companies::class;

    public function definition()
    {
        $users = User::factory()->create()->id;
        return [
            'companyName' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'website' => $this->faker->domainName,
            'logo' => $this->faker->name,
            'created_by_id' => $users,
            'updated_by_id' => $users
        ];
    }
}
