<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\User;
use App\Models\Companies;
use App\Models\Employees;

class EmployeesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = Employees::class;

    public function definition()
    {
        $users = User::factory()->create()->id;
        return [
            'firstname' => $this->faker->name(),
            'lastname' => $this->faker->name(),
            'companyId' => Companies::factory()->create()->companyId,
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => $this->faker->phoneNumber(),
            'created_by_id' => $users,
            'updated_by_id' => $users,
        ];
    }
}
