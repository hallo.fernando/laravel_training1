<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Sells;
use App\Models\Items;
use App\Models\Employees;

class SellsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = Sells::class;
    
    public function definition()
    {
        $items = Items::factory()->create();
        return [
            'date' => $this->faker->dateTime(),
            'itemId' => $items->itemId,
            'price' => $items->price,
            'discount' => $this->faker->numberBetween(1,10),
            'employeeId' => Employees::factory()->create()->employeeId
        ];
    }
}
