<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Summaries;
use App\Models\Sells;
use App\Models\Employees;

class SummariesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = Summaries::class;
    
    public function definition()
    {
        $sells = Sells::factory()->create();
        $price = $sells->price;
        $discount = $sells->discount;
        $pricetotal = empty($summaries)? 0 : $summaries->price_total;
        $finalpricetotal = $pricetotal + $price;
        $discountTotal = empty($summaries)? 0 : $summaries->discount_total;
        $finaldiscountTotal = $discountTotal + ($price * $discount / 100);
        $total = $finalpricetotal - $finaldiscountTotal;
        return [
            'date' => $sells->date, 
            'employeeId' => Employees::factory()->create()->employeeId,
            'price_total' => $finalpricetotal,
            'discount_total' => $finaldiscountTotal,
            'total' => $total
        ];
    }
}
