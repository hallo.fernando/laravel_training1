<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('password')->nullable()->change();
        });

        Schema::table('companies', function (Blueprint $table) {
            $table->unsignedBigInteger('created_by_id');
            $table->foreign('created_by_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('updated_by_id');
            $table->foreign('updated_by_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('employees', function (Blueprint $table) {
            $table->string('password')->nullable();
            $table->unsignedBigInteger('created_by_id');
            $table->foreign('created_by_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('updated_by_id');
            $table->foreign('updated_by_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('password')->nullable(false)->change();
        });

        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('created_by_id');
            $table->dropColumn('updated_by_id');
        });

        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn('created_by_id');
            $table->dropColumn('updated_by_id');
        });
    }
}
