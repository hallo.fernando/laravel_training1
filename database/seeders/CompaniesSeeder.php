<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Companies;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("public/seeders/companies.csv"), "r");
  
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                Companies::create([
                    "companyName" => $data['1'],
                    "email" => $data['2'],
                    "website" => $data['3'],
                    "logo" => $data['6'],
                    "created_by_id" => $data['4'],
                    "updated_by_id" => $data['5']
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csvFile);
    }
}
