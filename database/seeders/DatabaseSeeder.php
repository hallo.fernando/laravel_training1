<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CompaniesSeeder::class);
        $this->call(EmployeesSeeder::class);
        $this->call(ItemsSeeder::class);
        $this->call(SalesSummariesSeeder::class);
        $this->call(LanguageSeeder::class);
    }
}
