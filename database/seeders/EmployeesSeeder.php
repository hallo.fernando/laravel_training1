<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Employees;
use App\Models\Companies;

class EmployeesSeeder extends Seeder
{
    // protected $company;

    // public function __construct(){
    //     $this->company = Companies::select('companyId', 'companyName')->get();
    // }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("public/seeders/employees.csv"), "r");
  
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            // $companies = $this->company->where('companyName', $data['3'])->first();

            if (!$firstline) {
                Employees::create([
                    "firstname" => $data['1'],
                    "lastname" => $data['2'],
                    "companyId" => $data['3'],
                    // "companyId" => $companies->companyId,
                    "email" => $data['4'],
                    "phone" => $data['5'],
                    "password" => bcrypt($data['6']),
                    "created_by_id" => $data['7'],
                    "updated_by_id" => $data['8']
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csvFile);
    }
}
