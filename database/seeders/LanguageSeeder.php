<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\TranslationLoader\LanguageLine;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //companies
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'header',
            'text' => ['en' => 'CRUD Companies Data - Mini CRM', 'id' => 'CRUD Data Perusahaan - Mini CRM'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'crud',
            'text' => ['en' => 'CRUD Companies Data', 'id' => 'CRUD Nama Perusahaan'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'create',
            'text' => ['en' => 'Create Companies', 'id' => 'Tambah Perusahaan'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'edit',
            'text' => ['en' => 'Edit Companies', 'id' => 'Sunting Perusahaan'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'name',
            'text' => ['en' => 'Company Name', 'id' => 'Nama Perusahaan'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'select',
            'text' => ['en' => 'Select Companies', 'id' => 'Pilih Perusahaan'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'logo',
            'text' => ['en' => 'Logo', 'id' => 'Logo'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'upload',
            'text' => ['en' => 'Upload Logo', 'id' => 'Unggah Logo'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'latestimage',
            'text' => ['en' => 'Latest Logo Preview', 'id' => 'Pratinjau Logo Terakhir'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'newlogopreview',
            'text' => ['en' => 'New Logo Preview', 'id' => 'Pratinjau Logo Baru'],
        ]);

        //employees
        LanguageLine::create([
            'group' => 'employees',
            'key' => 'header',
            'text' => ['en' => 'CRUD Employees Data - Mini CRM', 'id' => 'CRUD Data Karyawan - Mini CRM'],
        ]);

        LanguageLine::create([
            'group' => 'employees',
            'key' => 'crud',
            'text' => ['en' => 'CRUD Employees Data', 'id' => 'CRUD Nama Karyawan'],
        ]);

        LanguageLine::create([
            'group' => 'employees',
            'key' => 'create',
            'text' => ['en' => 'Add Employees', 'id' => 'Tambah Karyawan'],
        ]);

        LanguageLine::create([
            'group' => 'employees',
            'key' => 'edit',
            'text' => ['en' => 'Edit Employees', 'id' => 'Sunting Karyawan'],
        ]);

        LanguageLine::create([
            'group' => 'employees',
            'key' => 'firstname',
            'text' => ['en' => 'First Name', 'id' => 'Nama Depan'],
        ]);

        LanguageLine::create([
            'group' => 'employees',
            'key' => 'lastname',
            'text' => ['en' => 'Last Name', 'id' => 'Nama Belakang'],
        ]);

        LanguageLine::create([
            'group' => 'employees',
            'key' => 'choose',
            'text' => ['en' => 'Choose Employees', 'id' => 'Pilih Karyawan'],
        ]);

        //items
        LanguageLine::create([
            'group' => 'items',
            'key' => 'header',
            'text' => ['en' => 'CRUD Items Data - Mini CRM', 'id' => 'CRUD Data Barang - Mini CRM'],
        ]);

        LanguageLine::create([
            'group' => 'items',
            'key' => 'price',
            'text' => ['en' => 'Price', 'id' => 'Harga'],
        ]);

        LanguageLine::create([
            'group' => 'items',
            'key' => 'name',
            'text' => ['en' => 'Items Name', 'id' => 'Nama Barang'],
        ]);

        LanguageLine::create([
            'group' => 'items',
            'key' => 'crud',
            'text' => ['en' => 'CRUD Items Data', 'id' => 'CRUD Nama Barang'],
        ]);

        LanguageLine::create([
            'group' => 'items',
            'key' => 'create',
            'text' => ['en' => 'Create Items', 'id' => 'Tambah Barang'],
        ]);

        LanguageLine::create([
            'group' => 'items',
            'key' => 'edit',
            'text' => ['en' => 'Edit Items', 'id' => 'Sunting Barang'],
        ]);

        LanguageLine::create([
            'group' => 'items',
            'key' => 'choose',
            'text' => ['en' => 'Choose Items', 'id' => 'Pilih Barang'],
        ]);

        //Sales
        LanguageLine::create([
            'group' => 'sales',
            'key' => 'header',
            'text' => ['en' => 'CRUD Sales Data - Mini CRM', 'id' => 'CRUD Data Penjualan - Mini CRM'],
        ]);

        LanguageLine::create([
            'group' => 'sales',
            'key' => 'discount',
            'text' => ['en' => 'Discount', 'id' => 'Diskon'],
        ]);

        LanguageLine::create([
            'group' => 'sales',
            'key' => 'crud',
            'text' => ['en' => 'CRUD Sales Data', 'id' => 'CRUD Nama Penjualan'],
        ]);

        LanguageLine::create([
            'group' => 'sales',
            'key' => 'create',
            'text' => ['en' => 'Create Sales', 'id' => 'Tambah Penjualan'],
        ]);

        LanguageLine::create([
            'group' => 'sales',
            'key' => 'edit',
            'text' => ['en' => 'Edit Sales', 'id' => 'Sunting Penjualan'],
        ]);

        LanguageLine::create([
            'group' => 'sales',
            'key' => 'orderlist',
            'text' => ['en' => 'Order List', 'id' => 'Daftar Pesanan'],
        ]);

        //Summaries
        LanguageLine::create([
            'group' => 'summaries',
            'key' => 'header',
            'text' => ['en' => 'Summaries Data - Mini CRM', 'id' => 'Rangkuman Data Penjualan - Mini CRM'],
        ]);

        LanguageLine::create([
            'group' => 'summaries',
            'key' => 'createdat',
            'text' => ['en' => 'Created At', 'id' => 'Dibuat pada'],
        ]);

        LanguageLine::create([
            'group' => 'summaries',
            'key' => 'lastupdate',
            'text' => ['en' => 'Last Update At', 'id' => 'Diperbaharui pada'],
        ]);

        LanguageLine::create([
            'group' => 'summaries',
            'key' => 'pricetotal',
            'text' => ['en' => 'Price Total', 'id' => 'Total Harga'],
        ]);

        LanguageLine::create([
            'group' => 'summaries',
            'key' => 'discount',
            'text' => ['en' => 'Discount Total', 'id' => 'Total Diskon'],
        ]);

        LanguageLine::create([
            'group' => 'summaries',
            'key' => 'total',
            'text' => ['en' => 'Total', 'id' => 'Total'],
        ]);

        LanguageLine::create([
            'group' => 'summaries',
            'key' => 'detail',
            'text' => ['en' => 'Summaries Detail', 'id' => 'Detail Penjualan'],
        ]);


        //General
        LanguageLine::create([
            'group' => 'general',
            'key' => 'back',
            'text' => ['en' => 'Back', 'id' => 'Kembali'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'view',
            'text' => ['en' => 'View', 'id' => 'Lihat'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'save',
            'text' => ['en' => 'Save Data', 'id' => 'Simpan Data'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'input',
            'text' => ['en' => 'Input Data', 'id' => 'Tambahkan Data'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'date',
            'text' => ['en' => 'Date', 'id' => 'Tanggal'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'home',
            'text' => ['en' => 'Home', 'id' => 'Beranda'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'choosefile',
            'text' => ['en' => 'Choose File', 'id' => 'Pilih File'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'import',
            'text' => ['en' => 'Import', 'id' => 'Impor'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'export',
            'text' => ['en' => 'Export', 'id' => 'Ekspor'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'close',
            'text' => ['en' => 'Close', 'id' => 'Tutup'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'search',
            'text' => ['en' => 'Search', 'id' => 'Cari'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'option',
            'text' => ['en' => 'Option', 'id' => 'Opsi'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'select',
            'text' => ['en' => 'Select', 'id' => 'Pilih'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'showing',
            'text' => ['en' => 'Showing', 'id' => 'Menampilkan'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'name',
            'text' => ['en' => 'Name', 'id' => 'Nama'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'to',
            'text' => ['en' => 'to', 'id' => 'hingga'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'of',
            'text' => ['en' => 'of', 'id' => 'dari'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'entries',
            'text' => ['en' => 'entries', 'id' => 'entri'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'createdat',
            'text' => ['en' => 'Created At', 'id' => 'Dibuat pada'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'updatedat',
            'text' => ['en' => 'Updated At', 'id' => 'Diperbaharui pada'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'website',
            'text' => ['en' => 'Website', 'id' => 'Situs'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'email',
            'text' => ['en' => 'Email', 'id' => 'Surel'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'phone',
            'text' => ['en' => 'Telephone', 'id' => 'Telepon'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'edit',
            'text' => ['en' => 'Edit', 'id' => 'Sunting'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'delete',
            'text' => ['en' => 'Delete', 'id' => 'Hapus'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'companies',
            'text' => ['en' => 'Companies', 'id' => 'Perusahaan'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'employees',
            'text' => ['en' => 'Employees', 'id' => 'Karyawan'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'items',
            'text' => ['en' => 'Items', 'id' => 'Barang'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'sales',
            'text' => ['en' => 'Sales', 'id' => 'Penjualan'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'summaries',
            'text' => ['en' => 'Summaries Data', 'id' => 'Rangkuman Penjualan'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'dashboard',
            'text' => ['en' => 'Dashboard', 'id' => 'Dasbor'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'successlogin',
            'text' => ['en' => 'You are logged in!', 'id' => 'Anda berhasil masuk!'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'startdate',
            'text' => ['en' => 'Start Date', 'id' => 'Dari Tanggal'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'enddate',
            'text' => ['en' => 'End Date', 'id' => 'Sampai Tanggal'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'reset',
            'text' => ['en' => 'Reset', 'id' => 'Atur Ulang'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'password',
            'text' => ['en' => 'Password', 'id' => 'Kata Sandi'],
        ]);

        LanguageLine::create([
            'group' => 'general',
            'key' => 'preview',
            'text' => ['en' => 'Preview', 'id' => 'Pratinjau'],
        ]);
    }
}
