<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Sells;
use App\Models\Employees;
use App\Models\Summaries;
use App\Models\Items;
use Carbon\Carbon;

class SalesSummariesSeeder extends Seeder
{
    // protected $employee;
    // protected $item;

    // public function __construct(){
    //     $this->item = Items::select('itemId', 'name')->get();
    //     $this->employee = Employees::select('employeeId', 'firstname')->get();
    // }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("public/seeders/salessummaries.csv"), "r");
  
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            // $items = $this->item->where('name', $data['2'])->first();
            // $employees = $this->employee->where('firstname', $data['5'])->first();

            if (!$firstline) {
                Sells::create([
                    "date" => $data['1'],
                    "itemId" => $data['2'],
                    // "itemId" => $items->itemId,
                    "price" => $data['3'],
                    "discount" => $data['4'],
                    "employeeId" => $data['5']
                    // "employeeId" => $employees->employeeId
                ]);

                $summaries = Summaries::where('date', '=', Carbon::parse($data['1'])->format('Y,m,d'))->where('employeeId', '=', $data['5'])->first();
                $pricetotal = empty($summaries)? 0 : $summaries->price_total;
                $finalpricetotal = $pricetotal + $data['3'];
                $discountTotal = empty($summaries)? 0 : $summaries->discount_total;
                $finaldiscountTotal = $discountTotal + ($data['4'] * $data['3'] /100);
                $total = $finalpricetotal - $finaldiscountTotal;

                Summaries::updateOrCreate(['date' => Carbon::parse($data['1'])->format('Y-m-d'),'employeeId' => $data['5']],
                    [
                    'price_total' => $finalpricetotal,
                    'discount_total' => $finaldiscountTotal,
                    'total' => $total
                    ]
                );
            }
            $firstline = false;
        }
   
        fclose($csvFile);
    }
}
