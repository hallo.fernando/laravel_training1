<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'           => 'Admin',
            'email'          => 'admin@admin.com',
            'password'       => bcrypt('password'),
        ]);

        User::create([
            'name'           => 'Eko Prastius',
            'email'          => 'eko@prastius.com',
            'password'       => bcrypt('password'),
        ]);
    }
}
