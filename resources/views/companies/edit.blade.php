<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Companies Data | CRM_Test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
</head>
<body>
@extends('layouts.app')
@section('content')
	<div class="container">
        <div class="card">
            <div class="card-header text-center">
                {{trans('companies.crud')}} - <strong>{{trans('companies.edit')}}</strong>
            </div>
            <div class="card-body">
                <a href="{{route('companies.index')}}" class="btn btn-primary">{{trans('general.back')}}</a>
                <br/>
                <br/>

				@if(count($errors) > 0)
				<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
				{{ $error }} <br/>
				@endforeach
				</div>
				@endif

                <form action="{{route('companies.update',$companies->companyId)}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group">
                        <label>{{trans('companies.name')}}</label>
                        <input type="text" name="companyName" required="required" class="form-control" value="{{ $companies->companyName }}" placeholder="{{trans('companies.name')}}...">
                    </div>
 
                    <div class="form-group">
                        <label>{{trans('general.email')}}</label>
						<input type="email" name="email" required="required" class="form-control" value="{{ $companies->email }}" placeholder="{{trans('general.email')}}...">
                    </div>

					<div class="form-group">
                        <label>{{trans('general.website')}}</label>
						<input type="text" name="website" required="required" class="form-control" value="{{ $companies->website }}" placeholder="{{trans('general.website')}}...">
                    </div>

                    <div class="form-group">
						<label><b>{{trans('companies.upload')}}</b></label><br/>
						<input type="file" name="logo" id="logo" class="form-control" >
					</div>

                    <div class="row form-group">
                        <div class="col-md-6">
						    <label><b>{{trans('companies.latestimage')}}</b></label><br/>
                            <img style="max-height: 150px;" src="{{ url('/storage/logo/'.$companies->logo) }}">
                        </div>
                        <div class="col-md-6">
                            <label><b>{{trans('companies.newlogopreview')}}</b></label><br/>
                            <img id="preview-image-before-upload" style="max-height: 150px;">
                        </div>
                    </div>

                    @php
                    $id = auth()->user()->id;
                    // print_r($id);
                    @endphp

                    <input type="text" name="updated_by_id" required="required" value="@php print_r($id) @endphp" class="form-control" hidden>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="{{trans('general.save')}}">
                    </div>
                </form>
            </div>
        </div>
    </div>

<script>
    $(document).ready(function (e) {
        $('#logo').change(function(){
            let reader = new FileReader();
            reader.onload = (e) => { 
            $('#preview-image-before-upload').attr('src', e.target.result); 
            }
        reader.readAsDataURL(this.files[0]); 
        });
    });
</script>


@endsection
 
</body>
</html>