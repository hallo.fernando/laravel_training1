<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Companies Data | CRM_Test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        <a href="{{route('home')}}" class="btn btn-primary">{{trans('general.home')}}</a>
        <br/>
        <br/>
        <div class="card">
            <div class="card-header text-center">
                {{trans('companies.header')}}
            </div>
            <div class="card-body">

                <a href="{{route('companies.create')}}" class="btn btn-primary">{{trans('general.input')}}</a>

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#importExcel">
                {{trans('general.import')}} Excel
                </button>

                <!-- Import Excel -->
                <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <form method="post" action="{{route('companies.import')}}" enctype="multipart/form-data">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{trans('general.import')}} Excel</h5>
                                </div>
                                <div class="modal-body">
        
                                    {{ csrf_field() }}
        
                                    <label>{{trans('general.choosefile')}} excel</label>
                                    <div class="form-group">
                                        <input type="file" name="file" required="required">
                                    </div>
        
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('general.close')}}</button>
                                    <button type="submit" class="btn btn-primary">{{trans('general.import')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <a href="{{route('companies.export')}}" class="btn btn-success my-3">{{trans('general.export')}} Excel</a>

                {{-- filter:start --}}
                <form class="row g-3" method="GET">

                    {{-- Date Range --}}
                    <div class="col-md-6">
                        <label>{{trans('general.startdate')}}</label>
                        <input type="date" id="Sstart" class="form-control" name="start_date" value="{{ request('start_date') }}">
                    </div>
                    <div class="col-md-6">
                        <label>{{trans('general.enddate')}}</label>
                        <input type="date" id="Send" class="form-control" name="end_date" value="{{ request('end_date') }}">
                    </div>

                    {{-- Select Companies Query --}}
                    <div class="col-md-2">
                        <select name="companies" id="Scompanies" class="form-select">
                            <option value="" selected>{{trans('companies.select')}}</option>
                            @foreach ($Optcompanies as $c)
                            <option value="{{ $c->companyName }}" {{ request('companies') == $c->companyName ? 'selected' : null }}>
                                {{ $c->companyName }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-2">
                        <input name="Semail" id="Semail" value="{{ request('Semail') }}" type="search" class="form-control"
                            placeholder="{{trans('general.email')}}...">
                    </div>
                    <div class="col-md-2">
                        <input name="Swebsite" id="Swebsite" value="{{ request('Swebsite') }}" type="search" class="form-control"
                            placeholder="{{trans('general.website')}}...">
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary mb-3">{{trans('general.search')}}</button>
                        <button type="submit" class="btn btn-danger mb-3 reset-btn">{{trans('general.reset')}}</button>
                    </div>
                </form>

                <table id="data_users_reguler" class="table table-striped display" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>{{trans('companies.name')}}</th>
                            <th>{{trans('general.email')}}</th>
                            <th>{{trans('general.website')}}</th>
                            <th>{{trans('companies.logo')}}</th>
                            <th>{{trans('general.createdat')}}</th>
                            <th>{{trans('general.updatedat')}}</th>
                            <th>{{trans('general.option')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php 
                        $i = 1;
                        @endphp
                        @foreach ($companies as $key => $c)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $c->companyName }}</td>
                            <td>{{ $c->email }}</td>
                            <td>{{ $c->website}}</td>
                            <td><img width="150px" src="{{ url('storage/logo/'.$c->logo) }}"></td>
                            <td>{{ $c->created_at}}</td>
                            <td>{{ $c->updated_at}}</td>
                            <td>
				                <a href="{{route('companies.edit', $c->companyId) }}" class="btn btn-warning">{{trans('general.edit')}}</a>
				                <a href="{{route('companies.delete', $c->companyId) }}" class="btn btn-danger">{{trans('general.delete')}}</a>
			                </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                    
            </div>
        </div>
    </div>

    <script>
    $(document).ready(function() {
   var table = $('#data_users_reguler').DataTable({searching: false});
} );

$('.reset-btn').on('click', function() {
    $('#Sstart').val(null).trigger("change");
    $('#Send').val(null).trigger("change");
    $('#Scompanies').val(null).trigger("change");
    $('#Semail').val(null).trigger("change");
    $('#Swebsite').val(null).trigger("change");
});
</script>

@endsection

</body>
</html>