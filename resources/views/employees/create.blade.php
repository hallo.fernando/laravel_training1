<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Employee Data | CRM_Test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
</head>
<body>
@extends('layouts.app')
@section('content')
	<div class="container">
        <div class="card">
            <div class="card-header text-center">
                {{trans('employees.crud')}} - <strong>{{trans('employees.create')}}</strong>
            </div>
            <div class="card-body">
                <a href="{{route('employees.index')}}" class="btn btn-primary">{{trans('general.back')}}</a>
                <br/>
                <br/>

				@if(count($errors) > 0)
				<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
				{{ $error }} <br/>
				@endforeach
				</div>
				@endif
                    
                <form action="{{route('employees.store')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label>{{trans('employees.firstname')}}</label>
                        <input type="text" name="firstname" required="required" class="form-control" placeholder="{{trans('employees.firstname')}}...">
                    </div>

					<div class="form-group">
                        <label>{{trans('employees.lastname')}}</label>
                        <input type="text" name="lastname" class="form-control" placeholder="{{trans('employees.lastname')}}...">
                    </div>

					<div class="form-group">
                        <label>{{trans('general.companies')}}</label>
						<select class="form-select" name="companyId" required>
							<option value="">{{trans('companies.select')}}</option>
                            @foreach ($companies as $c)
							<option value="{{ $c->companyId }}">{{ $c->companyName }}</option>
                            @endforeach
    					</select>
                    </div>
 
                    <div class="form-group">
                        <label>{{trans('general.email')}}</label>
						<input type="email" name="email" required="required" class="form-control" placeholder="{{trans('general.email')}}...">
                    </div>

                    <div class="form-group">
                        <label>{{trans('general.password')}}</label>
						<input type="password" name="password" required="required" class="form-control" placeholder="{{trans('general.password')}}...">
                    </div>

					<div class="form-group">
                        <label>{{trans('general.phone')}}</label>
						<input type="text" name="phone" required="required" class="form-control" placeholder="{{trans('general.phone')}}...">
                    </div>

                    @php
                    $id = auth()->user()->id;
                    // print_r($id);
                    @endphp

                    <input type="text" name="created_by_id" required="required" value="@php print_r($id) @endphp" class="form-control" hidden>
                    <input type="text" name="updated_by_id" required="required" value="@php print_r($id) @endphp" class="form-control" hidden>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="{{trans('general.save')}}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

 
</body>
</html>