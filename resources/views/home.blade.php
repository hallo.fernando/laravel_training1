@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="text-align:center;">{{trans('general.dashboard')}}</div>

                    <div class="card-body" style="text-align:center;">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{trans('general.successlogin')}}
                    </div>

                    <div class="card-body">
                    <div class="row">
                        <a href="{{route('companies.index')}}" class="btn btn-primary">CRUD {{trans('general.companies')}}</a>
                    </div>
                    <br/>
                    <div class="row">
                        <a href="{{route('employees.index')}}" class="btn btn-primary">CRUD {{trans('general.employees')}}</a>
                    </div>
                    <br/>
                    <div class="row">
                        <a href="{{route('items.index')}}" class="btn btn-primary">CRUD {{trans('general.items')}}</a>
                    </div>
                    <br/>
                    <div class="row">
                        <a href="{{route('sales.index')}}" class="btn btn-primary">CRUD {{trans('general.sales')}}</a>
                    </div>
                    <br/>
                    <div class="row">
                        <a href="{{route('summaries.index')}}" class="btn btn-primary">{{trans('general.summaries')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
