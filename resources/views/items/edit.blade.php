<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Items Data | CRM_Test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
</head>
<body>
@extends('layouts.app')
@section('content')
	<div class="container">
        <div class="card">
            <div class="card-header text-center">
                {{trans('items.crud')}} - <strong>{{trans('items.edit')}}</strong>
            </div>
            <div class="card-body">
                <a href="{{route('items.index')}}" class="btn btn-primary">{{trans('general.back')}}</a>
                <br/>
                <br/>

				@if(count($errors) > 0)
				<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
				{{ $error }} <br/>
				@endforeach
				</div>
				@endif

                <form action="{{route ('items.update',$items->itemId)}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group">
                        <label>{{trans('items.name')}}</label>
                        <input type="text" name="name" required="required" class="form-control" value="{{ $items->name }}" placeholder="{{trans('items.name')}}...">
                    </div>
 
                    <div class="form-group">
                        <label>{{trans('items.price')}}</label>
						<input type="number" name="price" required="required" class="form-control" value="{{ $items->price }}" placeholder="{{trans('items.price')}}...">
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="{{trans('general.save')}}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
 
</body>
</html>