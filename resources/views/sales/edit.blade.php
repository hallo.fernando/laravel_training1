<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sales Data | CRM_Test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
</head>
<body>
@extends('layouts.app')
@section('content')
	<div class="container">
        <div class="card">
            <div class="card-header text-center">
               {{trans('sales.crud')}} - <strong>{{trans('sales.edit')}}</strong>
            </div>
            <div class="card-body">
                <a href="{{route('sales.index')}}" class="btn btn-primary">{{trans('general.back')}}</a>
                <br/>
                <br/>

				@if(count($errors) > 0)
				<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
				{{ $error }} <br/>
				@endforeach
				</div>
				@endif

                <form action="{{route('sales.update',$sells->sellId)}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <input type="text" name="date" required="required" class="form-control" value="{{$sells->date}}" hidden>

                    <div class="form-group">
                        <label>{{trans('items.name')}}</label>
                        <select class="form-select counttotal" name="itemId" id="items" required>
                            <option value="">= {{trans('items.choose')}} =</option>
                            @foreach ($items as $item)
                            <option value="{{ $item->itemId }}"  {{ ( $item->itemId == $sells->itemId) ? 'selected' : '' }} data-price="{{$item->price}}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
 
                    <div class="form-group">
                        <label>{{trans('items.price')}}</label>
						<input type="number" name="price" required="required" class="form-control" value="{{$sells->price}}" readonly>
                    </div>

                    <div class="form-group">
                        <label>{{trans('sales.discount')}}</label>
						<input type="number" name="discount" id="discount" required="required" class="form-control counttotal" value="{{$sells->discount}}" placeholder="{{trans('sales.discount')}}...">
                    </div>

                    <div class="form-group">
                        <label>{{trans('general.employees')}}</label>
                        <select class="form-select" name="employeeId" required>
                            <option value="">{{trans('employees.choose')}}</option>
                            @foreach ($employees as $e)
                            <option value="{{ $e->employeeId }}"  {{ ( $e->employeeId == $sells->employeeId) ? 'selected' : '' }}>{{ $e->firstname ." ". $e->lastname }}</option>
                            @endforeach
                        </select>
                    </div>

                    <br/>
                    <h5>{{trans('summaries.pricetotal')}}: <span id="total">{{"Rp "}}{{$sells->price - ($sells->price * $sells->discount / 100)}}</span></h5>
                    <br/>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="{{trans('general.save')}}">
                    </div>
 
                </form>
            </div>
        </div>
    </div>

<script>
    $('.counttotal').on('change', function(){
    const price = $('#items option:selected').data('price');
    const discount = $("#discount").val() 

    const totalDiscount = (price * discount/100)
    const total = price - totalDiscount;
      
    $('[name=price]').val(price);
    $('#total').text(`Rp ${total}`);
    });
</script>

@endsection

</body>
</html>