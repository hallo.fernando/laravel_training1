<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sales Data | CRM_Test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        <a href="{{route('home')}}" class="btn btn-primary">{{trans('general.home')}}</a>
        <br/>
        <br/>
        <div class="card">
            <div class="card-header text-center">
                {{trans('sales.header')}}
            </div>
            <div class="card-body">
                <a href="{{route('sales.create')}}" class="btn btn-primary">{{trans('general.input')}}</a>
                <br/>
                <br/>

                {{-- filter:start --}}
                <form class="row g-3" method="GET">
                    {{-- Date Range --}}
                    <div class="col-md-6">
                        <label>{{trans('general.startdate')}}</label>
                        <input type="date" id="Sstart" class="form-control" name="start_date" value="{{ request('start_date') }}">
                    </div>
                    <div class="col-md-6">
                        <label>{{trans('general.enddate')}}</label>
                        <input type="date" id="Send" class="form-control" name="end_date" value="{{ request('end_date') }}">
                    </div>
                    
                    <div class="col-md-2">
                        <input name="Sname" id="Sname" value="{{ request('Sname') }}" type="search" class="form-control"
                            placeholder="{{trans('items.name')}}...">
                    </div>
                    <div class="col-md-2">
                        <input name="Sprice" id="Sprice" value="{{ request('Sprice') }}" type="search" class="form-control"
                            placeholder="{{trans('items.price')}}...">
                    </div>
                    <div class="col-md-2">
                        <input name="Sdiscount" id="Sdiscount" value="{{ request('Sdiscount') }}" type="search" class="form-control"
                            placeholder="{{trans('sales.discount')}}...">
                    </div>
                    <div class="col-md-2">
                        <input name="Semployees" id="Semployees" value="{{ request('Semployees') }}" type="search" class="form-control"
                            placeholder="{{trans('general.employees')}}...">
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary mb-3">{{trans('general.search')}}</button>
                        <button type="submit" class="btn btn-danger mb-3 reset-btn">{{trans('general.reset')}}</button>
                    </div>
                </form>

                <table id="data_users_reguler" class="table table-striped display" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>{{trans('general.date')}}</th>
                            <th>{{trans('items.name')}}</th>
                            <th>{{trans('items.price')}}</th>
                            <th>{{trans('sales.discount')}}</th>
                            <th>{{trans('general.employees')}}</th>
                            <th>{{trans('general.createdat')}}</th>
                            <th>{{trans('general.updatedat')}}</th>
                            <th>{{trans('general.option')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php 
                        $i = 1;
                        @endphp
                        @foreach ($sells as $key => $s)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $s->date }}</td>
                            <td>{{ $s->items->name }}</td>
                            <td>{{ number_format($s->items->price,2) }}</td>
                            <td>{{ $s->discount }}%</td>
                            <td>{{ $s->employees->firstname ." ". $s->employees->lastname }}</td>
                            <td>{{ $s->created_at}}</td>
                            <td>{{ $s->updated_at}}</td>
                            <td>
				                <a href="{{route('sales.edit', $s->sellId) }}" class="btn btn-warning">{{trans('general.edit')}}</a>
				                <a href="{{route('sales.delete', $s->sellId) }}" class="btn btn-danger">{{trans('general.delete')}}</a>
			                </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                    
            </div>
        </div>
    </div>

<script>
$(document).ready(function() {
   var table = $('#data_users_reguler').DataTable({searching:false});
} );

$('.reset-btn').on('click', function() {
    $('#Sstart').val(null).trigger("change");
    $('#Send').val(null).trigger("change");
    $('#Sname').val(null).trigger("change");
    $('#Sprice').val(null).trigger("change");
    $('#Sdiscount').val(null).trigger("change");
    $('#Semployees').val(null).trigger("change");
});
</script>

@endsection

</body>
</html>