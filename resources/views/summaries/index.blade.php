<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Summaries Data | CRM_Test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        <a href="{{route('home')}}" class="btn btn-primary">{{trans('general.home')}}</a>
        <br/>
        <br/>
        <div class="card">
            <div class="card-header text-center">
                {{trans('summaries.header')}}
            </div>
            <div class="card-body">

                {{-- filter:start --}}
                <form class="row g-3" method="GET">

                    {{-- Date Range --}}
                    <div class="col-md-6">
                        <label>{{trans('general.startdate')}}</label>
                        <input type="date" id="Sstart" class="form-control" name="start_date" value="{{ request('start_date') }}">
                    </div>
                    <div class="col-md-6">
                        <label>{{trans('general.enddate')}}</label>
                        <input type="date" id="Send" class="form-control" name="end_date" value="{{ request('end_date') }}">
                    </div>

                    {{-- Select Companies Query --}}
                    <div class="col-md-2">
                        <input class="form-control" id="Scompanies" name="Scompanies" list="listcompanies" value="{{ request('Scompanies') }}" placeholder="{{trans('companies.select')}}">
                        <datalist id="listcompanies">
                            @foreach ($Optcompanies as $c)
                            <option value="{{ $c->companyName }}" {{ request('Scompanies') === $c->companyName ? 'selected' : null }}>
                                {{ $c->companyName }}</option>
                            @endforeach
                        </datalist>
                    </div>

                    <div class="col-md-2">
                        <input name="Sfirstname" value="{{ request('Sfirstname') }}" type="search" id="Sfirst" class="form-control"
                            placeholder="{{trans('employees.firstname')}}...">
                    </div>

                    <div class="col-md-2">
                        <input name="Slastname" value="{{ request('Slastname') }}" type="search" id="Slast" class="form-control"
                            placeholder="{{trans('employees.lastname')}}...">
                    </div>

                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary mb-3">{{trans('general.search')}}</button>
                        <button type="submit" class="btn btn-danger mb-3 reset-btn">{{trans('general.reset')}}</button>
                    </div>
                </form>

                <table id="data_users_reguler" class="table table-striped display" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>{{trans('general.date')}}</th>
                            <th>{{trans('general.name')}}</th>
                            <th>{{trans('companies.name')}}</th>
                            <th>{{trans('summaries.createdat')}}</th>
                            <th>{{trans('summaries.lastupdate')}}</th>
                            <th>{{trans('summaries.pricetotal')}}</th>
                            <th>{{trans('summaries.discount')}}</th>
                            <th>{{trans('summaries.total')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php 
                        $i = 1;
                        use Carbon\Carbon;
                        @endphp
                        @foreach ($summaries as $key => $s)
                        <tr>
                            <td>{{ $i++  }}</td>
                            <td><a href="{{route('summaries.view', $s->summariesId) }}" class="btn btn-primary">{{ Carbon::parse($s->date)->format('Y-m-d') }}</a></td>
                            <td>{{ $s->employees->firstname ." ". $s->employees->lastname }}</td>
                            <td>{{ $s->employees->companies->companyName }}</td>
                            <td>{{ $s->created_at }}</td>
                            <td>{{ $s->updated_at }}</td>
                            <td>{{ number_format($s->price_total,2) }}</td>
                            <td>{{ number_format($s->discount_total,2) }}</td>
                            <td>{{ number_format($s->total,2) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<script>
$(document).ready(function() {
   var table = $('#data_users_reguler').DataTable({searching: false});
} );

$('.reset-btn').on('click', function() {
    $('#Sfirst').val(null).trigger("change");
    $('#Slast').val(null).trigger("change");
    $('#Scompanies').val(null).trigger("change");
    $('#Sstart').val(null).trigger("change");
    $('#Send').val(null).trigger("change");
});
</script>

@endsection
</body>
</html>