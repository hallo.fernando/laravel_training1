<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Summaries Data | CRM_Test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header text-center">
                {{trans('summaries.header')}}
            </div>
            <div class="card-body">

                <a href="/summaries" class="btn btn-primary">{{trans('general.back')}}</a>
                <br/>
                <br/>

                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane active" id="summaries-tab">
                            <!-- Start Here -->
                            <header class="header">
                                <div id="container-style" style="padding-bottom: 20px;">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="name-wrapper">
                                                <h1 style="text-align:center;" class="name">{{ $summaries->employees->firstname ." ". $summaries->employees->lastname }}</h1>
                                                <div class="headdetails">
                                                    <hr/>
                                                    <h6 style="text-align:center;" >{{trans('summaries.detail')}}</h3>
                                                    <hr/>
                                                </div>
                                            </div>

                                            <div class="summaries-details">
                                                <div class="row">
                                                    <div style="text-align:center; border-right:1px solid;" class="col-md-6">
                                                        <label>{{trans('companies.name')}}</label>
                                                        <p>{{ $summaries->employees->companies->companyName }}</p>
                                                    </div>
                                                    <div style="text-align:center;" class="col-md-6">
                                                        <label>{{trans('general.date')}}</label>
                                                        <p>{{date('Y-m-d', strtotime($summaries->date))}}</p>
                                                    </div>
                                                </div>

                                                <hr/>

                                                <div class="row">
                                                    <div style="text-align:center;">
                                                        <label>{{trans('sales.orderlist')}}</label>
                                                    </div>
                                                </div>

                                                <table id="order_data" class="table table-striped display" style="width:100%; margin-top:5px;">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>{{trans('items.name')}}</th>
                                                            <th>{{trans('items.price')}}</th>
                                                            <th>{{trans('sales.discount')}}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php 
                                                        $i = 1;
                                                        @endphp
                                                        @foreach ($sells as $key => $s)
                                                        <tr>
                                                            <td>{{ $i++ }}</td>
                                                            <td>{{ $s->items->name }}</td>
                                                            <td>{{ number_format($s->items->price,2) }}</td>
                                                            <td>{{ $s->discount }}%</td>
                                                        </tr>
                                                        @endforeach
                                                        {{-- <tr>
                                                            <td style="text-align:center;" colspan="2">Price Total</td>
                                                            <td>{{ $summaries->price_total }}</td>
                                                        </tr> --}}
                                                    </tbody>
                                                </table>

                                                <br/>

                                                <div class="row" style="text-align:center;">
                                                    <div class="col-md-4">
                                                        <label>{{trans('summaries.pricetotal')}}</label>
                                                        <input style="text-align:center;" type="text" class="form-control" value="{{ number_format($summaries->price_total,2) }}" readonly>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>{{trans('summaries.discount')}}</label>
                                                        <input style="text-align:center;" type="text" class="form-control" value="{{ number_format($summaries->discount_total,2) }}" readonly>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>{{trans('summaries.total')}}</label>
                                                        <input style="text-align:center;" type="text" class="form-control" value="{{ number_format($summaries->total,2) }}" readonly>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </header>
                            <!-- End Here -->
                        </div>
                    </div>
                </div>
            </div>
                    
            </div>
        </div>
    </div>
@endsection

    <script>
    $(document).ready(function() {
   var table = $('#order_data').DataTable({searching:false, lengthChange:false, paging: false, info:false});
} );
</script>

<!-- Length 5 Table Entries -->
<!-- <script>
    $(document).ready(function() {
   var table = $('#data_users_reguler').DataTable({
    pageLength : 5,
    lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']]
   });
} );
</script> -->

</body>
</html>