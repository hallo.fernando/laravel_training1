<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => 'guest'], function () {
    Route::get('/', function () {
        return view('auth/login');
    })->name('login');
});

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//companies
Route::get('/companies', [App\Http\Controllers\CompaniesController::class, 'index'])->name('companies.index');
Route::get('/companies/create', [App\Http\Controllers\CompaniesController::class, 'create'])->name('companies.create');
Route::post('/companies/store', [App\Http\Controllers\CompaniesController::class, 'store'])->name('companies.store');
Route::get('/companies/edit/{id}', [App\Http\Controllers\CompaniesController::class, 'edit'])->name('companies.edit');
Route::put('/companies/update/{id}', [App\Http\Controllers\CompaniesController::class, 'update'])->name('companies.update');
Route::get('/companies/delete/{id}', [App\Http\Controllers\CompaniesController::class, 'delete'])->name('companies.delete');
Route::get('/companies/export', [App\Http\Controllers\CompaniesController::class, 'export'])->name('companies.export');
Route::post('/companies/import', [App\Http\Controllers\CompaniesController::class, 'import'])->name('companies.import');

//employees
Route::get('/employees', [App\Http\Controllers\EmployeesController::class, 'index'])->name('employees.index');
Route::get('/employees/create', [App\Http\Controllers\EmployeesController::class, 'create'])->name('employees.create');
Route::post('/employees/store', [App\Http\Controllers\EmployeesController::class, 'store'])->name('employees.store');
Route::get('/employees/edit/{id}', [App\Http\Controllers\EmployeesController::class, 'edit'])->name('employees.edit');
Route::put('/employees/update/{id}', [App\Http\Controllers\EmployeesController::class, 'update'])->name('employees.update');
Route::get('/employees/delete/{id}', [App\Http\Controllers\EmployeesController::class, 'delete'])->name('employees.delete');
Route::get('/employees/export', [App\Http\Controllers\EmployeesController::class, 'export'])->name('employees.export');
Route::post('/employees/import', [App\Http\Controllers\EmployeesController::class, 'import'])->name('employees.import');

//items
Route::get('/items', [App\Http\Controllers\ItemsController::class, 'index'])->name('items.index');
Route::get('/items/create', [App\Http\Controllers\ItemsController::class, 'create'])->name('items.create');
Route::post('/items/store', [App\Http\Controllers\ItemsController::class, 'store'])->name('items.store');
Route::get('/items/edit/{id}', [App\Http\Controllers\ItemsController::class, 'edit'])->name('items.edit');
Route::put('/items/update/{id}', [App\Http\Controllers\ItemsController::class, 'update'])->name('items.update');
Route::get('/items/delete/{id}', [App\Http\Controllers\ItemsController::class, 'delete'])->name('items.delete');

//sales
Route::get('/sales', [App\Http\Controllers\SellsController::class, 'index'])->name('sales.index');
Route::get('/sales/create', [App\Http\Controllers\SellsController::class, 'create'])->name('sales.create');
Route::post('/sales/store', [App\Http\Controllers\SellsController::class, 'store'])->name('sales.store');
Route::get('/sales/edit/{id}', [App\Http\Controllers\SellsController::class, 'edit'])->name('sales.edit');
Route::put('/sales/update/{id}', [App\Http\Controllers\SellsController::class, 'update'])->name('sales.update');
Route::get('/sales/delete/{id}', [App\Http\Controllers\SellsController::class, 'delete'])->name('sales.delete');

//summaries
Route::get('/summaries', [App\Http\Controllers\SummariesController::class, 'index'])->name('summaries.index');
Route::get('/summaries/view/{id}', [App\Http\Controllers\SummariesController::class, 'view'])->name('summaries.view');

//email
Route::get('email-test', [App\Http\Controllers\JobController::class, 'enqueue'])->name('job.enqueue');

//lang
// if (file_exists(app_path('Http/Controllers/LocalizationController.php')))
//     Route::get('lang/{locale}', [App\Http\Controllers\LocalizationController::class , 'lang'])->name('lang');
// }

Route::get('lang/{locale}', [App\Http\Controllers\LocalizationController::class , 'lang'])->name('lang');

//timezone
Route::get('time/{time}', [App\Http\Controllers\TimezoneController::class , 'changetime'])->name('time');

