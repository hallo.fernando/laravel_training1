<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

// use Illuminate\Support\Facades\Artisan;
use App\Models\User;
use App\Models\Companies;
use App\Models\Employees;
use Laravel\Sanctum\HasApiTokens;

use JWTAuth;

class ApiTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    // protected function setUp():void{
    //     parent::setUp();
    //     Artisan::call('db:seed');
    // }

    //Just for learn, API TEST have been tested using Postman.

    /** @test */
    public function user_must_input_name_email_and_password_to_register()
    {
        $this->json('POST', 'api/register', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                'error' => [
                    'name',
                    'email',
                    'password'
                ]
            ]);
    }

    /** @test */
    public function user_register_successfully()
    {
        $payload = [
            'name' => 'Bill Gates',
            'email' => 'login@bill.com',
            'password' => 'password'
        ];

        $this->json('post', '/api/register', $payload, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                'success',
                'message',
                'data' => [
                    'name',
                    'email',
                    'updated_at',
                    'created_at',
                    'id',
                ]
            ]);
    }

    /** @test */
    public function user_must_enter_email_and_password_to_login()
    {
        $this->json('POST', 'api/login')
            ->assertStatus(200)
            ->assertJsonStructure([
                'error' => [
                    'email',
                    'password'
                ]
            ]);
    }

    /** @test */
    public function user_logins_successfully()
    {
        $user = User::factory()->create([
            'name' => 'Elon Musk',
            'email' => 'login@elon.com',
            'password' => bcrypt('password')
        ]);

        $payload = ['email' => 'login@elon.com', 'password' => 'password'];

        $this->json('POST', 'api/login', $payload, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                'success',
                'token'
            ]);
        
        $this->assertAuthenticated();
    }

    public function authenticate(){
        $user = User::factory()->create([
            'name' => 'Administrator',
            'email' => 'administrator@admin.com',
            'password' => bcrypt('password')
        ]);
        $this->user = $user;
        $token = JWTAuth::fromUser($user);
        return $token;
    }

    /** @test */
    public function get_users_data(){
        $token = $this->authenticate();
        
        $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('GET', 'api/get_user', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                'user' => [
                    'id',
                    'name',
                    'email',
                    'email_verified_at',
                    'created_at',
                    'updated_at'
                ],
            ]);
    }

    /** @test */
    public function user_can_view_companies_data()
    {
        $token = $this->authenticate();
        
        $companies = Companies::factory()->create([
            'companyId' => '56',
            'companyName' => 'Xiaomi',
            'email' => 'support@xiaomi.com',
            'website' => 'xiaomi.com',
            'logo' => 'xiaomi.jpg',
            'created_by_id' => $this->user->id,
            'updated_by_id' => $this->user->id
        ]);

        $employees = Employees::factory()->create([
            'firstname' => 'Lira',
            'lastname' => 'Kwok',
            'companyId' => $companies->companyId,
            'email' => 'lira@xiaomi.com',
            'phone' => '0812712831281',
            'created_by_id' => $this->user->id,
            'updated_by_id' => $this->user->id,
        ]);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('GET', '/api/companies', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([[
                'companyId' => 56,
                'companyName' => 'Xiaomi',
                'firstname' => 'Lira',
                'lastname' => 'Kwok',
                'email' => 'lira@xiaomi.com',
                'phone' => '0812712831281'
            ]]);
    }

    /** @test */
    public function user_can_view_specified_companies_data()
    {
        $token = $this->authenticate();
        
        $companies = Companies::factory()->create([
            'companyId' => '56',
            'companyName' => 'Xiaomi',
            'email' => 'support@xiaomi.com',
            'website' => 'xiaomi.com',
            'logo' => 'xiaomi.jpg',
            'created_by_id' => $this->user->id,
            'updated_by_id' => $this->user->id
        ]);

        $companies = Companies::factory()->create([
            'companyId' => '57',
            'companyName' => 'Infinix',
            'email' => 'support@infinix.com',
            'website' => 'infinix.com',
            'logo' => 'infinix.jpg',
            'created_by_id' => $this->user->id,
            'updated_by_id' => $this->user->id
        ]);

        $employees = Employees::factory()->create([
            'firstname' => 'Lira',
            'lastname' => 'Kwok',
            'companyId' => '57',
            'email' => 'lira@infinix.com',
            'phone' => '0812712831281',
            'created_by_id' => $this->user->id,
            'updated_by_id' => $this->user->id,
        ]);

        $employees = Employees::factory()->create([
            'firstname' => 'Army',
            'lastname' => 'Yunita',
            'companyId' => '57',
            'email' => 'army@infinix.com',
            'phone' => '0812667383933',
            'created_by_id' => $this->user->id,
            'updated_by_id' => $this->user->id,
        ]);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('GET', '/api/companies/57', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                [
                'companyId' => 57,
                'companyName' => 'Infinix',
                'firstname' => 'Lira',
                'lastname' => 'Kwok',
                ],
                [
                'companyId' => 57,
                'companyName' => 'Infinix',
                'firstname' => 'Army',
                'lastname' => 'Yunita',
                ]
        ]);
    }
    
}
