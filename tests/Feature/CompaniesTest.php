<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

//files
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

//excel
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\CompaniesImport;

use App\Models\Companies;
use App\Models\User;

class CompaniesTest extends TestCase
{
    Use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    protected $user;

    protected function setUp():void{
        parent::setUp();
        $this->user = User::factory()->create();
        $this->actingAs($this->user);
    }

    /** @test */
    public function user_can_read_companies_data()
    {
        $response = $this->get(route('companies.index'));
        $response->assertOk();
        $response->assertViewIs('companies.index');
    }

    /** @test */
    public function user_can_create_companies_data()
    {
        $response = $this->get(route('companies.create'));
        $response->assertOk();
        $response->assertViewIs('companies.create');
    }

    /** @test */
    public function user_can_store_companies_data()
    {
        $companies = Companies::factory()->create();
        $response = $this->post(route('companies.store'));

        $this->assertDatabaseHas('companies', [
            'companyName' => $companies->companyName,
            'email' => $companies->email,
            'website' => $companies->website
        ]);
    }

    /** @test */
    public function user_can_upload_companies_logo()
    {
        Storage::fake('public');
        
        $file = UploadedFile::fake()->image('logo.jpg');
        // $file->store('','public');

        $response = $this->post(route('companies.store'), [
            'companyName' => 'Xiaomi',
            'email' => 'support@xiaomi.com',
            'website' => 'xiaomi.com',
            'logo' => $file,
            'created_by_id' => $this->user->id,
            'updated_by_id' => $this->user->id
        ]);

        // Storage::disk('public')->assertExists('public/logo'.time()."_logo.jpg");
        Storage::disk('public')->assertExists('logo/'.$file->hashName());
    }

    /** @test */
    public function user_can_import_companies_excel()
    {
        // $file = UploadedFile::fake()->create('companies.xlsx');

        // Excel::fake();

        // $response = $this->post(route('companies.import'), [
        //     'file' => $file
        // ]);

        // Excel::assertImported($file->hashName(), 'public');


        Storage::fake('public');

        $file = UploadedFile::fake()->create('companies.xlsx');

        $response = $this->post(route('companies.import'), [
            'file' => $file
        ]);

        Storage::disk('public')->assertExists('companies_files/'.$file->hashName());
    }

    /** @test */
    public function user_can_edit_companies_data()
    {
        $companies = Companies::factory()->create();
        $response = $this->get(route('companies.edit', $companies->companyId));
        $response->assertOk();
        $response->assertViewIs('companies.edit');
    }

    /** @test */
    public function user_can_update_companies_data()
    {
        $companies = Companies::factory()->create();
        $companies->companyName = 'Rivenzon';
        $companies->email = 'support@river.com';
        $companies->website = 'rivenzon.com';
        $companies->logo = 'rivenzon.jpg';
        $companies->updated_by_id = $this->user->id;
        $companies->save();
        $response = $this->put(route('companies.update', $companies->companyId), $companies->toArray());

        $this->assertDatabaseHas('companies', [
            'companyName' => 'Rivenzon',
            'email' => 'support@river.com',
            'website' => 'rivenzon.com',
            'logo' => 'rivenzon.jpg',
            'updated_by_id' => $this->user->id
        ]);
    }

    /** @test */
    public function user_can_delete_companies_data()
    {
        $companies = Companies::factory()->create();
        $response = $this->get(route('companies.delete', $companies->companyId));

        $this->assertDatabaseMissing('companies', [
            'companyId' => $companies->companyId
        ]);
    }
    
}
