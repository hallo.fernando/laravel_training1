<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Bus;

use App\Mail\NotifyMail;

class EmailTest extends TestCase
{
    // use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    /** @test */
    public function mails_get_queued()
    {
        Mail::fake();

        // $toMail = 'recipient@example.com';
        $email = new NotifyMail();
        Mail::to('recipient@example.com')->cc('boss@example.com')->queue($email);

        Mail::assertQueued(NotifyMail::class, 1);
    }

    /** @test */
    public function mails_jobs_dispatched()
    {
        Bus::fake();

        // $toMail = 'recipient@example.com';
        $email = new NotifyMail();
        Mail::to('recipient@example.com')->cc('boss@example.com')->queue($email);

        dispatch(new NotifyMail($email));
        Bus::assertDispatched(NotifyMail::class);
    }
}
