<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

//files
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

use App\Models\Employees;
use App\Models\User;

class EmployeesTest extends TestCase
{
    Use DatabaseTransactions;

    /**
     * A basic feature test example.
     *
     * @return void
     */

    protected $user;

    protected function setUp():void{
        parent::setUp();
        $this->user = User::factory()->create();
        $this->actingAs($this->user);
    }

    /** @test */
    public function user_can_read_employees_data()
    {
        $response = $this->get(route('employees.index'));
        $response->assertOk();
        $response->assertViewIs('employees.index');
    }

    /** @test */
    public function user_can_create_employees_data()
    {
        $response = $this->get(route('employees.create'));
        $response->assertOk();
        $response->assertViewIs('employees.create');
    }

    /** @test */
    public function user_can_store_employees_data()
    {
        $employees = Employees::factory()->create();
        $response = $this->post(route('employees.store'));

        $this->assertDatabaseHas('employees', [
            'firstname' => $employees->firstname,
            'lastname' => $employees->lastname,
            'companyId' => $employees->companies->companyId,
            'email' => $employees->email,
            'phone' => $employees->phone
        ]);
    }

    /** @test */
    public function user_can_import_employees_excel()
    {
        Storage::fake('public');

        // $file = Excel::fake('companies.xlsx');
        $file = UploadedFile::fake()->create('employees.xlsx');

        // $file = UploadedFile::fake('companies.xlsx');
        // $file->store('companies_files','public');

        $response = $this->post(route('employees.import'), [
            'file' => $file
        ]);

        // Excel::assertImported('companies_files/'.$file->hashName(), 'public');

        // Storage::disk('public')->assertExists('public/logo'.time()."_logo.jpg");
        Storage::disk('public')->assertExists('employees_files/'.$file->hashName());
    }

    /** @test */
    public function user_can_edit_employees_data()
    {
        $employees = Employees::factory()->create();
        $response = $this->get(route('employees.edit', $employees->employeeId));
        $response->assertOk();
        $response->assertViewIs('employees.edit');
    }

    /** @test */
    public function user_can_update_employees_data()
    {
        $employees = Employees::factory()->create();
        $companies = \App\Models\Companies::factory()->create()->companyId;
        $employees->firstname = 'Albert';
        $employees->lastname = 'Riffenson';
        $employees->companyId = $companies;
        $employees->email = 'albert@rivenzon.com';
        $employees->phone = '081267363636';
        $employees->updated_by_id = $this->user->id;
        $employees->save();
        $response = $this->put(route('employees.update', $employees->employeeId), $employees->toArray());

        $this->assertDatabaseHas('employees', [
            'firstname' => 'Albert',
            'lastname' => 'Riffenson',
            'companyId' => $companies,
            'email' => 'albert@rivenzon.com',
            'phone' => '081267363636',
            'updated_by_id' => $this->user->id
        ]);
    }

    /** @test */
    public function user_can_delete_employees_data()
    {
        $employees = Employees::factory()->create();
        $response = $this->get(route('employees.delete', $employees->employeeId));

        $this->assertDatabaseMissing('employees', [
            'employeeId' => $employees->employeeId
        ]);
    }
}
