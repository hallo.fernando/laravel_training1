<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Items;
use App\Models\User;

class ItemsTest extends TestCase
{
    Use DatabaseTransactions;

    /**
     * A basic feature test example.
     *
     * @return void
     */

    protected $user;

    protected function setUp():void{
        parent::setUp();
        $this->user = User::factory()->create();
        $this->actingAs($this->user);
    }

    /** @test */
    public function user_can_read_items_data()
    {
        $response = $this->get(route('items.index'));
        $response->assertOk();
        $response->assertViewIs('items.index');
    }

    /** @test */
    public function user_can_create_items_data()
    {
        $response = $this->get(route('items.create'));
        $response->assertOk();
        $response->assertViewIs('items.create');
    }

    /** @test */
    public function user_can_store_items_data()
    {
        $items = Items::factory()->create();
        $response = $this->post(route('items.store'));

        $this->assertDatabaseHas('items', [
            'name' => $items->name,
            'price' => $items->price
        ]);
    }

    /** @test */
    public function user_can_edit_items_data()
    {
        $items = Items::factory()->create();
        $response = $this->get(route('items.edit', $items->itemId));
        $response->assertOk();
        $response->assertViewIs('items.edit');
    }

    /** @test */
    public function user_can_update_items_data()
    {
        $items = Items::factory()->create();
        $items->name = 'Steak Premium';
        $items->price = '300000';
        $items->save();
        $response = $this->put(route('items.update', $items->itemId), $items->toArray());

        $this->assertDatabaseHas('items', [
            'name' => 'Steak Premium',
            'price' => '300000',
        ]);
    }

    /** @test */
    public function user_can_delete_items_data()
    {
        $items = Items::factory()->create();
        $response = $this->get(route('items.delete', $items->itemId));

        $this->assertDatabaseMissing('items', [
            'itemId' => $items->itemId
        ]);
    }
}
