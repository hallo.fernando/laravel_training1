<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Sells;
use App\Models\User;

class SalesTest extends TestCase
{
    Use DatabaseTransactions;

    /**
     * A basic feature test example.
     *
     * @return void
     */

    protected $user;

    protected function setUp():void{
        parent::setUp();
        $this->user = User::factory()->create();
        $this->actingAs($this->user);
    }

    /** @test */
    public function user_can_read_sales_data()
    {
        $response = $this->get(route('sales.index'));
        $response->assertOk();
        $response->assertViewIs('sales.index');
    }

    /** @test */
    public function user_can_create_sales_data()
    {
        $response = $this->get(route('sales.create'));
        $response->assertOk();
        $response->assertViewIs('sales.create');
    }

    /** @test */
    public function user_can_store_sales_data()
    {
        $sells = Sells::factory()->create();
        $response = $this->post(route('sales.store'));

        $this->assertDatabaseHas('sells', [
            'date' => $sells->date,
            'itemId' => $sells->items->itemId,
            'price' => $sells->price,
            'discount' => $sells->discount,
            'employeeId' => $sells->employees->employeeId
        ]);
    }

    /** @test */
    public function user_can_edit_sales_data()
    {
        $sells = Sells::factory()->create();
        $response = $this->get(route('sales.edit', $sells->sellId));
        $response->assertOk();
        $response->assertViewIs('sales.edit');
    }

    /** @test */
    public function user_can_update_sales_data()
    {
        $sells = Sells::factory()->create();
        $items = \App\Models\Items::factory()->create();
        $employees = \App\Models\Employees::factory()->create()->employeeId;
        $sells->date = '2022-02-21 05:00:00';
        $sells->itemId = $items->itemId;
        $sells->price = $items->price;
        $sells->discount = '5';
        $sells->employeeId = $employees;
        $sells->save();
        $response = $this->put(route('sales.update', $sells->sellId), $sells->toArray());

        $this->assertDatabaseHas('sells', [
            'date' => '2022-02-21 05:00:00',
            'itemId' => $items->itemId,
            'price' => $items->price,
            'discount' => '5',
            'employeeId' => $employees
        ]);
    }

    /** @test */
    public function user_can_delete_sales_data()
    {
        $sells = Sells::factory()->create();
        $response = $this->get(route('sales.delete', $sells->sellId));

        $this->assertDatabaseMissing('sells', [
            'sellId' => $sells->sellId
        ]);
    }
}
