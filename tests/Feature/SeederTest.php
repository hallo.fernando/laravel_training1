<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

// use Illuminate\Foundation\Testing\DatabaseTransactions;

use Database\Seeders\UsersTableSeeder;
use Database\Seeders\CompaniesSeeder;
use Database\Seeders\EmployeesSeeder;
use Database\Seeders\ItemsSeeder;
use Database\Seeders\SalesSummariesSeeder;
use Database\Seeders\LanguageSeeder;

class SeederTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    /** @test */
    public function database_got_seeded_test()
    {
        // Run a specific seeder...
        $this->seed(UsersTableSeeder::class);
        $this->seed(CompaniesSeeder::class);
        $this->seed(EmployeesSeeder::class);
        $this->seed(ItemsSeeder::class);
        $this->seed(SalesSummariesSeeder::class);
        $this->seed(LanguageSeeder::class);
 
        $response = $this->get('/');
 
        $response->assertStatus(200);
    }
}
