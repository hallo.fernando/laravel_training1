<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Summaries;
use App\Models\User;

class SummariesTest extends TestCase
{
    Use DatabaseTransactions;

    /**
     * A basic feature test example.
     *
     * @return void
     */

    protected $user;

    protected function setUp():void{
        parent::setUp();
        $this->user = User::factory()->create();
        $this->actingAs($this->user);
    }

    /** @test */
    public function user_can_read_summaries_data()
    {
        $response = $this->get(route('summaries.index'));
        $response->assertOk();
        $response->assertViewIs('summaries.index');
    }

    /** @test */
    public function user_can_view_summaries_data()
    {
        $summaries = Summaries::factory()->create();
        $response = $this->get(route('summaries.view', $summaries->summariesId));
        $response->assertOk();
        $response->assertViewIs('summaries.view');
    }
}
